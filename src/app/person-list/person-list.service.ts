import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';

import { PersonListModel } from './person-list.model';
import { BaseListFilterModel } from '../shared/models/list.model';
import { environment } from 'src/environments/environment';

@Injectable()
export class PersonListService {

  constructor(private http: HttpClient) {
  }

  getPersons(model: BaseListFilterModel) {
    
    return this.http.post<PersonListModel>(environment.apiBaseUrl + '/person/getpersonlist', JSON.stringify(model));
  }

  deletePerson(id: number) {
    
    return this.http.post<boolean>(environment.apiBaseUrl + '/person/deleteperson/' + id.toString(), null);
  }
}
