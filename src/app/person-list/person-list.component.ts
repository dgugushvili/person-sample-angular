import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatDialog } from '@angular/material';
import { Router } from '@angular/router';

import { ConfirmDialogComponent } from '../shared/components/confirm-dialog/confirm-dialog.component';
import { PersonListService } from './person-list.service';
import { BaseListFilterModel } from '../shared/models/list.model';
import { PersonModel } from './person-list.model';

@Component({
  selector: 'app-person-list',
  templateUrl: './person-list.component.html',
  styleUrls: ['./person-list.component.scss']
})
export class PersonListComponent implements AfterViewInit {

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  displayedColumns: string[] = ['id', 'fullName', 'personalNumber', 'phoneNumber', 'birthDate', 'actions'];
  dataSource: MatTableDataSource<PersonModel>;

  constructor(
    private service: PersonListService, 
    public dialog: MatDialog,
    private router: Router) { }

  ngAfterViewInit() {

    this.updateDataSource();
  }

  onPageChanged() {

    this.updateDataSource();
  }

  updateDataSource() {

    this.service.getPersons(<BaseListFilterModel>{
      pageIndex: this.paginator.pageIndex,
      pageSize: this.paginator.pageSize
    }).subscribe(res => {

      this.dataSource = new MatTableDataSource<PersonModel>(res.data);

      this.paginator.length = res.totalCount;
    });
  }

  onCreate() {
    debugger;
    this.router.navigateByUrl('/person/edit');
  }

  onEdit(id?: number) {
    
    this.router.navigateByUrl('/person/edit/' + id);
  }

  onDelete(id: number) {

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '350px',
      height: '150px',
      autoFocus: false,
      restoreFocus: false,
      disableClose: true,
      data: "Are you sure you want to delete?"
    });

    dialogRef.afterClosed().subscribe(res => {

      if (res)
        this.service.deletePerson(id).subscribe(res => {

          if (res)
            this.updateDataSource();
        });
    });
  }
}