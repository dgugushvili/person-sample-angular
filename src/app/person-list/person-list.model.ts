import { BaseListModel } from '../shared/models/list.model';

export interface PersonModel {

    id: number;
    firstName: string;
    lastName: string;
    personalNumber: string;
    phoneNumber: string;
    birthDate: Date;
}

export interface PersonListModel extends BaseListModel<PersonModel> { }