import { NotificationModel } from './notification.model';

export interface ResponseModel<T> {
    
    value: T;
    notifications: NotificationModel[];
}