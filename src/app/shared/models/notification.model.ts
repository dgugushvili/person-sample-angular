export interface NotificationModel {

    type: NotificationType;
    message: string;
}

export enum NotificationType {
    
    info = 0,
    success = 1,
    warning = 2,
    error = 3
}