export interface BaseListFilterModel {
    
    pageIndex: number;
    pageSize: number;
}

export interface BaseListModel<T> {
    
    data: T[];
    totalCount: number;
}