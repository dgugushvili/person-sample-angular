import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpInterceptor, HttpRequest, HttpEvent, HttpHandler } from "@angular/common/http";

@Injectable()
export class ContentTypeInterceptor implements HttpInterceptor {

  constructor() {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      
    request = request.clone({
      headers: request.headers.set("Content-Type", "application/json")
    });
    
    return next.handle(request);
  }
}
