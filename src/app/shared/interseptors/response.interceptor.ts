import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpInterceptor, HttpRequest, HttpEvent, HttpHandler, HttpResponse } from "@angular/common/http";
import { HttpHeaders } from "@angular/common/http";
import { HttpParams } from "@angular/common/http";
import { map, filter, catchError, mergeMap, tap } from 'rxjs/operators'
import { ResponseModel } from '../models/response.model';

@Injectable()
export class ResponseInterceptor implements HttpInterceptor {

  constructor() {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    return next.handle(request).pipe(
      map(res => {

        if (res instanceof HttpResponse) {
          
          if (res.body && res.ok) {
            
            var responce = res.body as ResponseModel<any>;

            return res.clone({
              body: responce.value
            });
          }
        }
      }),
      //   catchError(err => of([]))
    );
  }
}
