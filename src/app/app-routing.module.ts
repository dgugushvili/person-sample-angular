import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PersonListComponent } from './person-list/person-list.component';
import { PersonEditComponent } from './person-edit/person-edit.component';
import { LayoutComponent } from './shared/components/layout/layout.component';
import { AppComponent } from './app.component';

const routes: Routes = [{
  path: '',
  component: AppComponent,
  children: [
    {
      path: '',
      component: LayoutComponent,
      children: [
        {
          path: 'persons',
          component: PersonListComponent,
          data: { title: 'Person List' }
        },
        { 
          path: 'person/edit/:id',
          component: PersonEditComponent,
          data: { title: 'Edit' }
        },
        { 
          path: 'person/edit',
          component: PersonEditComponent,
          data: { title: 'Create' }
        },
        { 
          path: '', 
          redirectTo: '/persons', 
          pathMatch: 'full' 
        }
      ]
    },
    { 
      path: '**', 
      redirectTo: '/persons', 
      pathMatch: 'full'
    }
  ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }